#!/usr/bin/bash

OLD=$(pwd)

KernelPermission()
{
	local RAWFILE='/opt/basicnetwork/bin/bntrc'

	if [[ -z `getcap $RAWFILE 2>/dev/null` ]]
		then
			sudo setcap cap_net_raw+eip $RAWFILE
		fi

	RAWFILE='/opt/basicnetwork/bin/bnfpn'

	if [[ -z `getcap $RAWFILE 2>/dev/null` ]]
		then
			sudo setcap cap_net_raw+eip $RAWFILE
		fi
 }

CreateDir()
{
	if [[ ! -d $1 ]]
		then
			sudo install -o root -g root -dm 755 "$1"
		fi
  }

MoveFiles()
{
	sudo cp -p $@ ; (($?)) || echo "--> kopyalandı ${2} ${1##*/}" 
}

install()
{
	local DIR=$(readlink -f ${0%/*}) ; cd "$DIR"

	find . -maxdepth 1 -not -path '*/\.*' -type d -exec realpath {} \; | \
	grep -wv "${DIR}$" | while read IFL
		do
			local AF=$(find $IFL -type f )

			for CD in $AF
				do
					local MD=$(dirname $CD | sed "s%${DIR}%%")
					CreateDir $MD
					MoveFiles $CD $MD
				done
		done

	KernelPermission
  }
  
uninstall()
{
	if [[ -d /opt/basicnetwork ]]
		then
			sudo rm -rf /opt/basicnetwork
			(($?)) || echo "'/opt/basicnetwork' silindi"
		fi

	if [[ -x /usr/bin/basicnetwork ]]
		then
			sudo rm -fv /usr/bin/basicnetwork
		fi
  }

	case $1 in
		0 ) uninstall ;;
		1 ) install ;;
	esac

	cd "$OLD"
