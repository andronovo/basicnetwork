# BasicNetwork

	
==== Bashrc alias tanım şekli ( örn: net adı için ) ( isteğe bağlı ) =====================================

		alias net=basicnetwork $@


	
==== Bağımlılıklar =======================================================================

		bash networkmanager iproute2 wget


	
==== Kur\Kaldır =========================================================================

		git clone https://bitbucket.org/andronovo/basicnetwork.git

		basicnetwork/install.sh 1	# Kur
		basicnetwork/install.sh 0	# Kaldır


	
==== Kullanım ==========================================================================

		basicnetwork	-->  Bağlantı bilgilerini içeren sayfayı başlatır

		basicnetwork w --> Kablosuz bağlantı menüsünü başlatır

		basicnetwork s --> Aynı ağa bağlı cihaz listesi menüsünü başlatır

		basicnetwork k --> Mevcut alanda kanal frekans yoğunluğunu test eder

		basicnetwork t --> Servis sağlayıcıların iletişim bilgilerine ait liste gösterir


	
==== Ayrıntılar ==========================================================================

		basicnetwork  	 -->	Hotspot bağlantılarda bağlanılan cihazın markası görüntülenmez.

									Aynı anda kablo ve kablosuz bağlı ise modem bilgisi için alınacak değerler 
									kablo bağlantısı üzerinden sağlanır.

									Aygıt başlık rengi yeşil		; Aygıt modeme bağlı ve internet erişimi var
									Aygıt başlık rengi kırmızı		; Aygıt modeme bağlı fakat internet erişimi yok.
																				(ağ geçidi gibi sorunlar var)

									Modem başlık rengi yeşil	; Modem bağlı ve internete çıkış yapıyor
									Modem başlık rengi kırmızı	; Modem bağlı fakat dış IP almıyor.

==============

		basicnetwork w --> 	"[1] bağlan"  seçeneği doğrudan seçilirse ağ adının elle girilmesi gerekir. 

									Eğer "[1] bağlan" seçeneğinden önce fare ile bağlantı adı seçilmiş ise ad otomatik girilir ve 
									kayıtl bir ağ değilse sadece şifre istenir. Kayıtlı bir ağ ise doğrudan bağlantı kurulur.

									Wifi aygıtı kapalı veya buradan kapatılmış ise, Wifi seçeneği seçildiğinde otomatik olarak açlır.

									Kablolu ve Kablosuz hiçbir aygıt, herhangi bir ağa bağlı değilse ve Wifi donanımı var ise
									başlangıç ekranı Kablosuz bağlantı menüsünü olacaktır.
==============

		basicnetwork s -->	Subnetmask bağlı bulunulan ağa göre otomatik seçilir. Harhangi bir dhcp aygıtına bağlı değilse, 
									C sınıfı (0/24) standardına göre tarama yapılır.

									"s" değerine 0 ile 255 arası değer eklenirse, mevcut gateway adres kolonunun sondan 3. değeri,
									girilen değer olarak tarama yapılacaktır. 
									Örn: Gateway tarafından 192.168.1.1 adresi verilmiş ve komut  " basicnetwork s 0 " şeklinde verilmişse
									tarama işlemi 192.168.0.0/24 şeklinde yapılır.

									Bağlı bulunulan Gateway'in tamamen dışında özel bir adres taranacak ise, bu adresin tamamı veya 
									ilk 3 değeri girilebilir.
									Örn: Gateway tarafından 192.168.1.1 adresi verilmiş ve komut  " basicnetwork s 10.0.0 " veya
									" basicnetwork s 10.0.0.1 " şeklinde verilmişse, tarama işlemi 10.10.0.0/24 şeklinde yapılır.

									Aygıt tarama sonucunda bazı kısaltmalar
									===============================

									(Gw) (Gateway) Dhcp aygıtı, modem.
									(Co)	(Connect) Dhcp'nin modem tarafından verildiği, fakat bu modeme access point üzerinden
															bağlanılması durumunda, bağlanılan access point 'i gösterir.
									(Lc)	(Local)		Tarama işlemini yapan bilgisayarın kendi ağ kartı /ları
									(Cb)	(Cable)		Aygıt kablo ile bağlanmış. ( Tahmini )
									(Wf)	(Wifi)			Aygıt kablosuz bağlanmış. ( Tahmini )
									(Sl)	(Sleep)		Aygıt bağlı fakat ağı şuan kullanmıyor. Uyku moduna girmiş aygıtlarda görünür.
==============

		basicnetwork k --> Test işlemi 10 ile 99 arası değer girilmemiş ise, varsayılan 30 saniye için yapılır.

									En uygun sonuç, taramanın uzun birkaç sonucunda alınacaktır

									Renk = Yoğunluk
									==============

									Açık yeşil		:	Kanal boş, (en uygun kanal)
									Koyu yeşil		:	Bu kablosuz ağa bağlı ve kanal sadece bu dağtıcı tarafından kullanılıyor. (kanal uygun)
									Koyu mavi	:	Bu kablosuz ağa bağlı ve bu kanal başka bir dağıtıcı ile çakışıyor. (boş yok ise uygun)
									Mor				:	Bu kanal 1 adet dağıtıcı tarafından kullanılyor (tercihen kullanılabilir)
									Açık kırmızı	: 	Bu kanal 2 adet dağıtıcı tarafından kullanılyor (mecbur ise kullanılabilir)
									Koyu kırmızı	: 	Bu kanal aşırı yoğun. (yapacak bir şey yok durumu)
										
									İlave olarak tarama sırasında ara değerler oluşur. Kanalda yayın yapan aygıtın yakınlığı buna göre
									tesbit edilebilir.
									   

